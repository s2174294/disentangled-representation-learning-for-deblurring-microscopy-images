import os
import torch
from torch.utils.data import Dataset
import random

from utils import preprocess_image

dirname = os.getcwd()
savedir = os.path.join(dirname, "../saved_datasets")


class SampleDataset(Dataset):
    def __init__(self, path_dict, labels, image_batch_size, focus_list, minimum_diff, maximum_diff):
        self.labels = labels
        self.path_dict = path_dict
        self.batch_size = image_batch_size
        self.focus_list = focus_list
        self.minimum_diff = minimum_diff
        self.maximum_diff = maximum_diff

    def __len__(self):
        return len(self.path_dict)

    def __getitem__(self, idx):
        label = self.labels[idx]
        in_focus_level = self.focus_list[idx]
        imgs, focus_diffs = self.sample_images(label, in_focus_level)
        labels = [label] * self.batch_size
        return imgs, labels, focus_diffs

    def sample_images(self, label, in_focus_level):
        paths = self.path_dict[label]
        sampled_paths = random.sample(paths, self.batch_size)
        focus_diffs = []
        for path in sampled_paths:
            _, z_level = os.path.split(path)
            z_level = int(z_level[:-4])
            focus_diff = z_level - in_focus_level
            focus_diff = 2 * ((focus_diff - self.minimum_diff) / (self.maximum_diff - self.minimum_diff)) - 1

            focus_diffs.append(focus_diff)
        imgs = [preprocess_image(path) for path in sampled_paths]

        return imgs, focus_diffs

