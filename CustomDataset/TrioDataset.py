import os
from torch.utils.data import Dataset
import random
from numpy import interp

from utils import preprocess_image

dirname = os.getcwd()
savedir = os.path.join(dirname, "../saved_datasets")
folder_brightfield = "../CroppedData/Brightfield"


class TrioDataset(Dataset):
    def __init__(self, path_dict, labels):
        self.labels = labels
        self.path_dict = path_dict

    def __len__(self):
        total_length = 0
        for l in self.path_dict.values():
            total_length += len(l)
        return total_length

    def __getitem__(self, idx):
        real_index = int(interp(idx, [0, self.__len__() - 1], [0, len(self.labels) - 1]))
        label_1 = self.labels[real_index]
        path_3 = ""

        found_match = False

        while not found_match:
            img_1, focus_diff_1, img_2, focus_diff_2 = self.sample_images(label_1)

            for i in range(50):
                label_2 = self.labels[random.randint(0, len(self.labels) - 1)]

                day, crop = label_2.split('-')
                data_path = os.path.join(dirname, folder_brightfield, day, crop, 'data')
                path_3 = os.path.join(data_path, focus_diff_1 + '.tif')
                if os.path.exists(path_3):
                    found_match = True
                    break

        img_3 = preprocess_image(path_3)
        focus_diff_3 = path_3.split("/")[-1][:-4]

        return img_1, img_2, img_3, focus_diff_1, focus_diff_2, focus_diff_3, label_1, label_2

    def sample_images(self, label):
        paths = self.path_dict[label]
        sampled_paths = random.sample(paths, 2)
        path_1 = sampled_paths[0]
        path_2 = sampled_paths[1]

        img_1 = preprocess_image(path_1)
        focus_diff_1 = path_1.split("\\")[-1][:-4]

        img_2 = preprocess_image(path_2)
        focus_diff_2 = path_2.split("\\")[-1][:-4]
        return img_1, focus_diff_1, img_2, focus_diff_2
