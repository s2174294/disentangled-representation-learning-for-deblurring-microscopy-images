"""
Class used to load the data during training
Images are only loaded in memory when needed during training to avoid memory overloading
"""

# Third party imports
import os
from torch.utils.data import Dataset
import random

# First party imports
from utils import preprocess_image

dirname = os.path.dirname(__file__)


class CustomDataset(Dataset):
    """
    Custom Dataset to load data during training
    """
    def __init__(self, path_dict, in_focus_path_dict, labels, image_batch_size, focus_list, minimum_diff, maximum_diff):
        """
        Initializer of the CustomDataset
        :param path_dict:
            - Dictionary mapping labels of images to paths, avoids having to look up image paths during training
        :param in_focus_path_dict:
            - Dictionary mapping labels of images to paths of the in-focus images
        :param labels:
            - Available labels of images in the dataset
        :param image_batch_size:
            - Batch size, amount of images that are randomly sampled for each label
        :param focus_list:
            - List of in-focus z-levels for each of the labels
        :param minimum_diff:
            - Maximum z-level difference occuring in the dataset, used for min/max normalization
        :param maximum_diff:
            - Minimum z-level difference occuring in the dataset, used for min/max normalization
        """
        self.labels = labels
        self.path_dict = path_dict
        self.in_focus_path_dict = in_focus_path_dict
        self.batch_size = image_batch_size
        self.focus_list = focus_list
        self.minimum_diff = minimum_diff
        self.maximum_diff = maximum_diff

    def __len__(self):
        """
        Method returning the length of the dataset
        :return: The amount of labels in the dataset
        """
        return len(self.path_dict)

    def __getitem__(self, idx):
        """
        Method used to sample an item from the dataset
        For each label, self.batch_size numebr of crops with different z-levels are randomly sampled

        :param idx: Index of the data point requested
        :return:
        """
        label = self.labels[idx]
        in_focus_level = self.focus_list[idx]
        in_focus_img = preprocess_image(self.in_focus_path_dict[label])
        imgs, focus_diffs = self.sample_images(label, in_focus_level)
        labels = [label] * self.batch_size
        return imgs, in_focus_img, labels, focus_diffs

    def sample_images(self, label, in_focus_level):
        """
        Method used to sample crops at different z-levels, preprocess them and record their z-level difference with the in-focus image
        :param label:
            - Label for which crops need to be sampled
        :param in_focus_level:
            - The z-level of the in-focus image for the given label
        :return:
            - imgs: List of preprocessed images (pytorch tensors of 128x128x1) at different z-levels
            - focus_diffs: List of the z-level differences of all crops in imgs
        """
        paths = self.path_dict[label]
        sampled_paths = random.sample(paths, self.batch_size)
        focus_diffs = []
        for path in sampled_paths:
            _, z_level = os.path.split(path)
            z_level = int(z_level[:-4])
            focus_diff = z_level - in_focus_level
            focus_diff = 2 * ((focus_diff - self.minimum_diff) / (self.maximum_diff - self.minimum_diff)) - 1
            focus_diffs.append(focus_diff)
        imgs = [preprocess_image(path) for path in sampled_paths]
        return imgs, focus_diffs
