import os
from torch.utils.data import Dataset
import random

from utils import preprocess_image

dirname = os.getcwd()


class SampleDataset(Dataset):
    def __init__(self, path_dict, labels):
        self.labels = labels
        self.path_dict = path_dict

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, idx):
        label = self.labels[idx]
        paths = self.path_dict[label]
        path = random.sample(paths, 1)[0]
        img = preprocess_image(path)
        return img, label

    def get_num_classes(self):
        return len(self.labels)