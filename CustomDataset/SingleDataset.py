"""
Class used to load images one-by-one
Images are only loaded in memory when needed to avoid memory overloading
"""

# Third party imports
import os
from torch.utils.data import Dataset
import random
from numpy import interp

# First party imports
from utils import preprocess_image

dirname = os.path.dirname(__file__)


class SingleDataset(Dataset):
    def __init__(self, path_dict, labels, batch_size):
        self.path_dict = path_dict
        self.labels = labels
        self.batch_size = batch_size

    def __len__(self):
        return len(self.labels)
        # total_length = 0
        # for l in self.path_dict.values():
        #     total_length += len(l)
        # return total_length

    def __getitem__(self, index):
        # real_index = int(interp(index, [0, self.__len__() - 1], [0, len(self.labels) - 1]))
        label = self.labels[index]
        imgs = []
        focus_diffs = []
        for i in range(self.batch_size):
            img, focus_diff = self.sample_image(label)
            imgs.append(img)
            focus_diffs.append(focus_diff)

        labels = [label] * self.batch_size
        return imgs, focus_diffs, labels

    def sample_image(self, label):
        paths = self.path_dict[label]
        path = random.sample(paths, 1)[0]
        img = preprocess_image(path)
        focus_diff = path.split("\\")[-1][:-4]

        return img, focus_diff
