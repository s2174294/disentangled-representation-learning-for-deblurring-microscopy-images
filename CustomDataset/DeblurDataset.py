import os

from torch.utils.data import Dataset
import random
from utils import preprocess_image

dirname = os.path.dirname(__file__)
savedir = os.path.join(dirname, "../saved_datasets")
folder_brightfield = os.path.join(dirname, "../CroppedData/Brightfield")

class DeblurDataset(Dataset):
    def __init__(self, path_dict, labels):
        self.labels = labels
        self.path_dict = path_dict

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, idx):
        label_1 = self.labels[idx]
        day_1, crop_1 = label_1.split('-')

        label_2 = self.labels[random.randint(0, len(self.labels) - 1)]
        day_2, crop_2 = label_2.split('-')

        paths = self.path_dict[label_1]
        path_1 = random.sample(paths, 1)[0]

        path_2 = os.path.join(dirname, folder_brightfield, day_2, crop_2, 'target', 'InFocus.tif')

        path_3 = os.path.join(dirname, folder_brightfield, day_1, crop_1, 'target', 'InFocus.tif')

        img_1 = preprocess_image(path_1)
        img_2 = preprocess_image(path_2)
        img_3 = preprocess_image(path_3)

        return img_1, img_2, img_3, label_1, label_2

    def show_labels(self):
        return self.labels