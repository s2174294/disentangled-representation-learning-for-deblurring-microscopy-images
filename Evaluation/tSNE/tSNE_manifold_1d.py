import os
import random

import torch
from sklearn.manifold import TSNE
import pandas as pd
import seaborn as sns
import argparse

from torch.utils.data import DataLoader
import matplotlib.cm as cmx
import matplotlib.pyplot as plt

from CustomDataset.SingleDataset import SingleDataset

from Models.manifold_one_dimensional import Encoder
from utils import get_grouped_path_dict

savedir = os.path.join(os.getcwd(), '../../ModelSaves')

parser = argparse.ArgumentParser()
parser.add_argument('--cuda', type=bool, default=False, help="run the following code on a GPU")
parser.add_argument('--batch_size', type=int, default=30, help="batch size")
parser.add_argument('--encoder_save', type=str, default='encoder_1d', help="model save for encoder")

FLAGS = parser.parse_args(args=[])

if __name__ == '__main__':
    """
    Load Encoder and decoder model
    """
    encoder = Encoder()

    encoder.load_state_dict(torch.load(os.path.join(savedir, FLAGS.encoder_save), map_location=torch.device('cpu')))

    """
    Setup data loading
    """

    path_dict, labels = get_grouped_path_dict("brightfield")
    data = SingleDataset(path_dict, labels, FLAGS.batch_size)
    loader = DataLoader(data, batch_size=FLAGS.batch_size, shuffle=True)

    amount_of_batches = 6
    images = []
    focuses = []
    labelling = []
    indexes = random.sample(range(0, len(data)), amount_of_batches)

    for i in indexes:
        image_batch, focus_diff_batch, labels_batch = data.__getitem__(i)
        image_batch = torch.stack(image_batch)
        images.append(image_batch)
        focuses += list(focus_diff_batch)
        labelling += list(labels_batch)

    images = torch.cat(images, dim=0)

    focuses = [int(focus) for focus in list(focuses)]
    label_list = [label.split('location_')[1] for label in labelling]

    blur_code, id_code = encoder(images)

    cmapping = cmx.get_cmap()

    perplexities = [10]

    for perplexity in perplexities:

        # Identity tSNE

        model = TSNE(n_components=2, init='pca', perplexity=perplexity, random_state=0)
        identity_tsne = model.fit_transform(id_code.data.numpy())

        fig, ax = plt.subplots(1)
        tsne_1 = identity_tsne[:, 0]
        tsne_2 = identity_tsne[:, 1]

        tsne_result_df = pd.DataFrame({'tsne_1': tsne_1, 'tsne_2': tsne_2, 'label': label_list})

        sns.scatterplot(x='tsne_1', y='tsne_2', hue='label', data=tsne_result_df, ax=ax, s=120)
        ax.set_xlim((tsne_1.min() - 5, tsne_1.max() + 5))
        ax.set_ylim((tsne_2.min() - 5, tsne_2.max() + 5))
        ax.set_aspect('equal')
        ax.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.0)
        plt.title(f'tSNE Identity, perplexity = {perplexity}')
        plt.tight_layout()
        plt.show()

        # Blur visualization 1D
        # No tSNE is used, since blur latent code is 1D

        fig, ax = plt.subplots(1)
        tsne_1 = blur_code.detach().numpy()
        tsne_1 = tsne_1.reshape((tsne_1.shape[0],))

        tsne_result_df = pd.DataFrame({'tsne_1': tsne_1, 'tsne_2': tsne_1, 'label': focuses})

        minimum = min(focuses)
        maximum = max(focuses)

        sns.scatterplot(x='tsne_1', y='tsne_2', hue='label', data=tsne_result_df, ax=ax, s=120, palette='RdBu')
        lim = (tsne_1.min() - 1, tsne_1.max() + 1)
        norm = plt.Normalize(minimum, maximum)
        sm = plt.cm.ScalarMappable(cmap="RdBu", norm=norm)
        ax.set_xlabel('blur code')
        ax.set_ylabel('blur code')
        sm.set_array([])
        ax.set_xlim(lim)
        ax.set_ylim(lim)
        ax.set_aspect('equal')
        ax.get_legend().remove()
        ax.figure.colorbar(sm)
        # ax.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.0)
        plt.title(f'Blur latent code values')
        plt.grid()
        plt.tight_layout()

        plt.show()
