import os
import torch
import matplotlib.pyplot as plt

savedir = os.path.join(os.getcwd(), '../../MLVAE-scores')

# Insert ml_vae_score training monitor here
monitor_1 = torch.load(os.path.join(savedir, "mlvae_mlvae_score"))
label_1 = ""

blur_acc = []
blur_cr_loss = []
id_acc = []
id_cr_loss = []
size = 300
indexes = []
for i in range(1, size):
    indexes.append(i)
    blur_acc.append(monitor_1[i-1][0])
    blur_cr_loss.append(monitor_1[i-1][1])
    id_acc.append(monitor_1[i-1][2])
    id_cr_loss.append(monitor_1[i-1][3])

label_blur = "Blur Loss"
label_id = "ID Loss"

plt.plot(indexes, blur_cr_loss, linewidth=1.0, color="r", label=label_blur)
plt.plot(indexes, id_cr_loss, linewidth=1.0, color="b", label=label_id)
plt.legend()
plt.grid()
plt.title("Cross Entropy Loss (latent code and ID prediction)")
plt.xlabel("Epochs")
plt.ylabel("Cross entropy loss")
plt.tight_layout()
plt.show()

label_blur="Blur latent code"
label_id="ID latent code"

plt.plot(indexes, blur_acc, linewidth=1.0, color="r", label=label_blur)
plt.plot(indexes, id_acc, linewidth=1.0, color="b", label=label_id)
plt.legend()
plt.grid()
plt.title("Prediction accuracy")
plt.xlabel("Epochs")
plt.ylabel("Accuracy")
plt.tight_layout()
plt.show()


