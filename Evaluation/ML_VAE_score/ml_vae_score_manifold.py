import os
import argparse
from itertools import cycle

import torch
import torch.nn as nn
import torch.optim as optim

from torch.utils.data import DataLoader, RandomSampler
from Models.manifold import Encoder, Decoder
from CustomDataset.EvalSampleDataset import SampleDataset

from Models.classifiers import Classifier
from utils import get_path_dict_and_labels, weights_initialization, get_path_dict_and_labels_and_focus_list

savedir = os.path.join(os.getcwd(), '../../ModelSaves')

parser = argparse.ArgumentParser()
# add arguments
parser.add_argument('--cuda', type=bool, default=False, help="run the following code on a GPU")
parser.add_argument('--accumulate_evidence', type=str, default=False, help="accumulate class evidence before producing swapped images")

parser.add_argument('--batch_size', type=int, default=25, help="batch size for training")
parser.add_argument('--beta_1', type=float, default=0.9, help="default beta_1 val for adam")
parser.add_argument('--beta_2', type=float, default=0.999, help="default beta_2 val for adam")

parser.add_argument('--encoder_save', type=str, default='encoder', help="model save for encoder")
parser.add_argument('--decoder_save', type=str, default='decoder', help="model save for decoder")

parser.add_argument('--initial_learning_rate', type=float, default=0.001, help="starting learning rate")
parser.add_argument('--end_epoch', type=int, default=400, help="Number of epochs")

FLAGS = parser.parse_args(args=[])

RUN_ID = "manifold_model"

if __name__ == "__main__":
    encoder = Encoder()
    encoder.load_state_dict(torch.load(os.path.join(savedir, FLAGS.encoder_save), map_location=torch.device('cpu')))

    decoder = Decoder()
    decoder.load_state_dict(torch.load(os.path.join(savedir, FLAGS.decoder_save), map_location=torch.device('cpu')))

    if FLAGS.cuda:
        encoder.cuda()
        decoder.cuda()

    path_dict, labels, focus_list = get_path_dict_and_labels_and_focus_list("brightfield")
    data = SampleDataset(path_dict, labels)
    total_label_list = labels
    num_classes = data.get_num_classes()

    sampler = RandomSampler(data, replacement=True, num_samples=1000)
    loader = DataLoader(data, sampler=sampler, batch_size=FLAGS.batch_size)

    blur_classifier = Classifier(dim=32768, num_classes=num_classes)
    blur_classifier.apply(weights_initialization)

    identity_classifier = Classifier(dim=32768, num_classes=num_classes)
    identity_classifier.apply(weights_initialization)

    if FLAGS.cuda:
        blur_classifier.cuda()
        identity_classifier.cuda()

    cross_entropy_loss = nn.CrossEntropyLoss()

    blur_classifier_optimizer = optim.Adam(
        list(blur_classifier.parameters()),
        lr=FLAGS.initial_learning_rate,
        betas=(FLAGS.beta_1, FLAGS.beta_2)
    )

    identity_classifier_optimizer = optim.Adam(
        list(identity_classifier.parameters()),
        lr=FLAGS.initial_learning_rate,
        betas=(FLAGS.beta_1, FLAGS.beta_2)
    )

    decayRate = 0.98
    lr_scheduler_blur = torch.optim.lr_scheduler.ExponentialLR(optimizer=blur_classifier_optimizer, gamma=decayRate)
    lr_scheduler_id = torch.optim.lr_scheduler.ExponentialLR(optimizer=identity_classifier_optimizer, gamma=decayRate)

    savedmodels = os.path.join(os.getcwd(), "../../TrainedModels")

    training_monitor = torch.zeros(FLAGS.end_epoch, 4)

    for epoch in range(0, FLAGS.end_epoch):
        print(f'Epoch #{epoch} ..........................................................................')
        term1_epoch = 0
        term2_epoch = 0
        term3_epoch = 0
        term4_epoch = 0
        for it, (image_batch, labels_batch) in enumerate(loader):
            if it % 5 == 0 or epoch == 0:
                print(f'Train iteration {it}')
            if FLAGS.cuda:
                X = image_batch.cuda()
            else:
                X = image_batch

            class_labels = torch.tensor([total_label_list.index(el) for el in labels_batch])

            if FLAGS.cuda:
                blur_code, id_code = encoder(X.cuda())
                class_labels = class_labels.cuda()
            else:
                blur_code, id_code = encoder(X)

            blur_latent_embeddings = blur_code

            identity_latent_embeddings = id_code

            blur_classifier_optimizer.zero_grad()

            # Blur

            blur_classifier_pred = blur_classifier(blur_latent_embeddings)

            blur_classification_error = cross_entropy_loss(blur_classifier_pred, class_labels)
            blur_classification_error.backward(retain_graph=True)

            _, blur_classifier_pred = torch.max(blur_classifier_pred, 1)
            blur_classifier_accuracy = (blur_classifier_pred.data == class_labels).sum().item() / X.size(dim=0)

            blur_classifier_optimizer.step()

            # Identity

            identity_classifier_optimizer.zero_grad()

            identity_classifier_pred = identity_classifier(identity_latent_embeddings)
            identity_classification_error = cross_entropy_loss(identity_classifier_pred, class_labels)
            identity_classification_error.backward()

            _, identity_classifier_pred = torch.max(identity_classifier_pred, 1)
            identity_classifier_accuracy = (identity_classifier_pred.data == class_labels).sum().item() / X.size(dim=0)

            identity_classifier_optimizer.step()

            term1_epoch += blur_classifier_accuracy
            term2_epoch += blur_classification_error.detach()
            term3_epoch += identity_classifier_accuracy
            term4_epoch += identity_classification_error.detach()

        print("Blur classifier accuracy:  %.3f" % (term1_epoch / (it + 1)))
        print("Blur cross-entropy loss: %.3f" % (term2_epoch / (it + 1)))
        print("ID classifier accuracy:  %.3f" % (term3_epoch / (it + 1)))
        print("ID cross-entropy loss:  %.3f" % (term4_epoch / (it + 1)))
        print('\n')

        if epoch % 5 == 0:
            print("LR Blur: ", lr_scheduler_blur.get_last_lr())
            print("LR ID: ", lr_scheduler_id.get_last_lr())
            # print("REG LR: ", lr_scheduler_reg.get_last_lr())
            lr_scheduler_blur.step()
            lr_scheduler_id.step()

        training_monitor[epoch, :] = torch.FloatTensor(
            [term1_epoch / (it + 1), term2_epoch / (it + 1), term3_epoch / (it + 1), term4_epoch / (it + 1)])
        if os.path.exists(os.path.join(savedmodels, RUN_ID + f'_training_monitor_e{epoch - 1}')):
            os.remove(os.path.join(savedmodels, RUN_ID + f'_training_monitor_e{epoch - 1}'))
        torch.save(training_monitor, os.path.join(savedmodels, RUN_ID + '_training_monitor_e%d' % epoch))

        if (epoch + 1) % 10 == 0 or (epoch + 1) == FLAGS.end_epoch:
            print("SAVE")
            torch.save(blur_classifier.state_dict(),
                       os.path.join(savedmodels, RUN_ID + '_' + 'blur_classifier' + f'_e{epoch}'))
            torch.save(identity_classifier.state_dict(),
                       os.path.join(savedmodels, RUN_ID + '_' + 'id_classifier' + f'_e{epoch}'))