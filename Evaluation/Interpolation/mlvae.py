import argparse

import torch
import os

from numpy import linspace
import matplotlib.pyplot as plt
from Models.MLVAE import VAEEncoder, VAEDecoder

from utils import preprocess_image, get_grouped_path_dict

savedir = os.path.join(os.getcwd(), '../../ModelSaves')

parser = argparse.ArgumentParser()

# add arguments
parser.add_argument('--cuda', type=bool, default=False, help="run the following code on a GPU")

# paths to save models
parser.add_argument('--encoder_save', type=str, default='encoder_mlvae', help="model save for encoder")
parser.add_argument('--decoder_save', type=str, default='decoder_mlvae', help="model save for decoder")

FLAGS = parser.parse_args(args=[])

encoder = VAEEncoder()
encoder.load_state_dict(torch.load(os.path.join(savedir, FLAGS.encoder_save), map_location=torch.device('cpu')))

decoder = VAEDecoder()
decoder.load_state_dict(torch.load(os.path.join(savedir, FLAGS.decoder_save), map_location=torch.device('cpu')))

"""
Linear interpolation
"""

normalizing = False

img1 = preprocess_image("../../EvalData/Brightfield/23_08_2022_location_17/crop_9/data/-40.tif")
img2 = preprocess_image("../../EvalData/Brightfield/23_08_2022_location_17/crop_9/data/40.tif")

blur_code_1, _, id_code_1, _ = encoder(img1.view(1, 1, 128, 128))
blur_code_2, _, id_code_2, _ = encoder(img2.view(1, 1, 128, 128))

ratios = linspace(0, 1, num=7)

with torch.no_grad():
    for ratio in ratios:
        blur_code = (1.0 - ratio) * blur_code_1 + ratio * blur_code_2

        id_code = id_code_1

        ratio_string = "{:.2f}".format(ratio)

        decoded_image = decoder(blur_code, id_code)
        img_tensor = decoded_image.squeeze(dim=0).permute(1, 2, 0)
        fig = plt.imshow(img_tensor, cmap='gray', vmin=0, vmax=1)
        plt.title(f"Ratio: {ratio_string}")
        fig.axes.get_xaxis().set_visible(False)
        fig.axes.get_yaxis().set_visible(False)
        plt.show()


