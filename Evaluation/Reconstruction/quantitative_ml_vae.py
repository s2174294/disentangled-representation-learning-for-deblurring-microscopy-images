from torch.utils.data import DataLoader, RandomSampler
import argparse
from Models.MLVAE import VAEEncoder, VAEDecoder
from CustomDataset.EvalSampleDataset import SampleDataset
import torch
import os
from torchmetrics import StructuralSimilarityIndexMeasure, PeakSignalNoiseRatio
from utils import get_grouped_path_dict

savedir = os.path.join(os.getcwd(), '../../ModelSaves')

parser = argparse.ArgumentParser()

# add arguments
parser.add_argument('--cuda', type=bool, default=False, help="run the following code on a GPU")

# paths to save models
parser.add_argument('--encoder_save', type=str, default='encoder_mlvae', help="model save for encoder")
parser.add_argument('--decoder_save', type=str, default='decoder_mlvae', help="model save for decoder")

FLAGS = parser.parse_args(args=[])

encoder = VAEEncoder()
encoder.load_state_dict(torch.load(os.path.join(savedir, FLAGS.encoder_save), map_location=torch.device('cpu')))

decoder = VAEDecoder()
decoder.load_state_dict(torch.load(os.path.join(savedir, FLAGS.decoder_save), map_location=torch.device('cpu')))

ssim = StructuralSimilarityIndexMeasure(data_range=1.0)
psnr = PeakSignalNoiseRatio(data_range=1.0)

batch_size = 64

path_dict, labels = get_grouped_path_dict("brightfield")
split = int(0.2 * len(labels))

train_labels = labels[split:]
print(train_labels)


test_labels = labels[:split]
print(test_labels)

train_data = SampleDataset(path_dict, train_labels)
sampler = RandomSampler(train_data, replacement=True, num_samples=1000)
train_loader = DataLoader(train_data, sampler=sampler, batch_size=batch_size)

test_data = SampleDataset(path_dict, test_labels)
test_sampler = RandomSampler(test_data, replacement=True, num_samples=1000)
test_loader = DataLoader(test_data, sampler=test_sampler, batch_size=batch_size)

ssim_avg = 0
psnr_avg = 0
with torch.no_grad():
    for k in range(5):
        ssim_iter = 0
        psnr_iter = 0
        for it, (image_batch, labels_batch) in enumerate(train_loader):
            if it % 20 == 0:
                print(f"it: {it}")
            targets = image_batch
            blur_code, _, id_code, _ = encoder(targets)
            preds = decoder(blur_code, id_code)
            # print(ssim(targets, preds))
            ssim_iter += ssim(targets, preds).detach()
            psnr_iter += psnr(targets, preds).detach()
            # print(ssim_iter / (it + 1))

        ssim_avg += ssim_iter / (it + 1)
        psnr_avg += psnr_iter / (it + 1)

ssim_avg = ssim_avg / 5
psnr_avg = psnr_avg / 5

print(f"SSIM average training: {ssim_avg}")
print(f"PSNR average training: {psnr_avg}")

ssim_avg = 0
psnr_avg = 0
for k in range(5):
    ssim_iter = 0
    psnr_iter = 0
    for it, (image_batch, labels_batch) in enumerate(test_loader):
        if it % 20 == 0:
            print(f"it: {it}")
        targets = image_batch
        blur_code, _, id_code, _ = encoder(targets)
        preds = decoder(blur_code, id_code)
        ssim_iter += ssim(targets, preds).detach()
        psnr_iter += psnr(targets, preds).detach()

    ssim_avg += ssim_iter / (it + 1)
    psnr_avg += psnr_iter / (it + 1)
ssim_avg = ssim_avg / 5
psnr_avg = psnr_avg / 5

print(f"SSIM average testing: {ssim_avg}")
print(f"PSNR average testing: {psnr_avg}")