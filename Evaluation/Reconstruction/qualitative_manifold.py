import argparse
from Models.manifold import Encoder, Decoder
import torch
import os
import matplotlib.pyplot as plt
from utils import preprocess_image, reparameterize

parser = argparse.ArgumentParser()

savedir = os.path.join(os.getcwd(), '../../ModelSaves')

# add arguments
parser.add_argument('--cuda', type=bool, default=False, help="run the following code on a GPU")

# paths to save models
parser.add_argument('--encoder_save', type=str, default='encoder', help="model save for encoder")
parser.add_argument('--decoder_save', type=str, default='decoder', help="model save for decoder")

FLAGS = parser.parse_args(args=[])

encoder = Encoder()
encoder.load_state_dict(torch.load(os.path.join(savedir, FLAGS.encoder_save), map_location=torch.device('cpu')))

decoder = Decoder()
decoder.load_state_dict(torch.load(os.path.join(savedir, FLAGS.decoder_save), map_location=torch.device('cpu')))

if FLAGS.cuda:
    encoder.cuda()
    decoder.cuda()

datadir = "../../EvalData"

# Edit path
path = os.path.join(datadir, "Brightfield/23_08_2022_location_17/crop_1/data/15.tif")
img = preprocess_image(path)
fig = plt.imshow(img.permute(1, 2, 0), cmap='gray', vmin=0, vmax=1)
fig.axes.get_xaxis().set_visible(False)
fig.axes.get_yaxis().set_visible(False)
plt.show()

with torch.no_grad():
    # print(img)
    blur_code, id_code = encoder(img.view(1, img.size(0), img.size(1), img.size(2)))

    img_recon = decoder(blur_code, id_code)

    new_img = img_recon.squeeze(dim=0).permute(1, 2, 0)
    fig = plt.imshow(new_img, cmap='gray', vmin=0, vmax=1)
    fig.axes.get_xaxis().set_visible(False)
    fig.axes.get_yaxis().set_visible(False)
    plt.show()


