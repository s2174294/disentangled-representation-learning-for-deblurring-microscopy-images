import argparse

import torch
import os
import matplotlib.pyplot as plt

from Models.manifold_one_dimensional import Encoder, Decoder

from utils import preprocess_image

in_focus_path = "../../EvalData/Brightfield/23_08_2022_location_17/crop_0/target/InFocus.tif"
blurry_path = "../../EvalData/Brightfield/23_08_2022_location_17/crop_9/"

focus_level = "35"

savedir = os.path.join(os.getcwd(), '../../ModelSaves')

parser = argparse.ArgumentParser()

# add arguments
parser.add_argument('--cuda', type=bool, default=False, help="run the following code on a GPU")

# paths to save models
parser.add_argument('--encoder_save', type=str, default='encoder_1d', help="model save for encoder")
parser.add_argument('--decoder_save', type=str, default='decoder_1d', help="model save for decoder")

FLAGS = parser.parse_args(args=[])

encoder = Encoder()
encoder.load_state_dict(torch.load(os.path.join(savedir, FLAGS.encoder_save), map_location=torch.device('cpu')))

decoder = Decoder()
decoder.load_state_dict(torch.load(os.path.join(savedir, FLAGS.decoder_save), map_location=torch.device('cpu')))

with torch.no_grad():

    img_in_focus = preprocess_image(in_focus_path)

    img_tens = img_in_focus.permute(1, 2, 0)
    fig = plt.imshow(img_tens, cmap='gray', vmin=0, vmax=1)
    fig.axes.get_xaxis().set_visible(False)
    fig.axes.get_yaxis().set_visible(False)
    plt.title('In focus image')
    plt.tight_layout()
    plt.show()

    print("blurry image:")
    blurry_image = preprocess_image(blurry_path + f"data/{focus_level}.tif")
    img_tens = blurry_image.permute(1, 2, 0)
    fig = plt.imshow(img_tens, cmap='gray', vmin=0, vmax=1)
    fig.axes.get_xaxis().set_visible(False)
    fig.axes.get_yaxis().set_visible(False)
    plt.title('Blurry image')
    plt.tight_layout()
    plt.show()

    blur_code_1, id_code_1 = encoder(img_in_focus.view(1, 1, 128, 128))
    blur_code_2, id_code_2 = encoder(blurry_image.view(1, 1, 128, 128))

    recon = decoder(blur_code_1, id_code_2)
    print("Recon:")
    img_tens = recon.squeeze(dim=0).permute(1, 2, 0)
    fig = plt.imshow(img_tens, cmap='gray', vmin=0, vmax=1)
    fig.axes.get_xaxis().set_visible(False)
    fig.axes.get_yaxis().set_visible(False)
    plt.title('Deblurred image')
    plt.tight_layout()
    plt.show()

    print("Expected Image")
    img_expect = preprocess_image(blurry_path + "target/InFocus.tif")

    img_tens = img_expect.permute(1, 2, 0)
    fig = plt.imshow(img_tens, cmap='gray', vmin=0, vmax=1)
    fig.axes.get_xaxis().set_visible(False)
    fig.axes.get_yaxis().set_visible(False)
    plt.title('Expected image')
    plt.tight_layout()
    plt.show()
