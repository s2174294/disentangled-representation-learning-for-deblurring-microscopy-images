# A disentangled representation learning approach for deblurring microscopy images

This codebase is a result of the Master Thesis titled 'A Disentangled Representation Learning Approach for Deblurring Microscopy Images', conducted by Stan Ritsema.
The aim of the research is to disentangle between blur and identity in the latent space. 
Three approaches are compared: ML-VAE (https://github.com/DianeBouchacourt/multi-level-vae/tree/master) and a manifold learning based approach. 

## Installing required packages


To install the required packages, first create a virtual environment by running ``python -m venv env``
Afterwards, activate the virtual environment.
Then, simply run ``pip install -r requirements.txt``. 

## Preprocessing

The following folder should be created with microscopy images. 

```
Codebase 
│
└───Dataset
    ┣─── Z_stack_day1_location_1
    │    │ Z-Stack - Trans - 001.tif
    │    │ Z-Stack - Trans - 002.tif
    │    │ ...
    │    │ Z-Stack - Trans - 100.tif
    │    │ Z-Stack - Trans - InFocus.tif
    ┣─── Z_stack_day1_location_2
    │    │ Z-Stack - Trans - 001.tif
    │    │ Z-Stack - Trans - 002.tif
    │    │ ...
    │    │ Z-Stack - Trans - 100.tif
    │    │ Z-Stack - Trans - InFocus.tif
    ┣─── Z_stack_day2_location_1
    │    │ ...
    ┣─── Z_stack_day2_location_2
    │    │ ...
```

After which running preprocessing.py generates the correct data structure in the ``./CroppedData`` folder.
The data is resized, cropped to images of 128x128 and split in a data and target folder for each crop. The target folder contains the in-focus image, Projection image and the In Focus z-level.  
This results into the following folder structure:

```
Codebase 
│
└───CroppedData
    ┣─── Brightfield
    │    ┣─── day_1_location_1
    │    │    ┣─── crop_0
    │    │    │    ┣─── data
    │    │    │    │    │ 0.tif
    │    │    │    │    │ 1.tif
    │    │    │    │    │ ...
    │    │    │    │    │ -41.tif
    │    │    │    ┣─── target
    │    │    │    │    │ InFocus.tif
    │    │    │    │    │ Projection.tif
    │    │    │    │    │ z_level_focus.txt
    │    │    ┣─── crop_1
    │    │    │ ... 
    │    ┣─── day_1_location_2
    │    │ ...                           
```

## Training 

After preprocessing is finished, a model with one of three approaches can be trained. ML-VAE, Manifold and Manifold 1D. 
The files to train these models are stored in the ``./Training`` folder. For each approach, there is a separate file, to avoid having to manually change the imports. 
In the files, parameters like learning rate, batch size and epoch amount can be modified.

## Evaluation 

For evaluating the models, multiple metrics are stored in the ``./Evaluation`` folder.
Three pre-trained models are stored in the ``./ModelSaves`` folder.  
These models are trained on the following z-stacks:

```
Z_Stack_23_08_2022_Day4_Dish0_random_location_17
Z_Stack_24_08_2022_Day5_Dish0_random_location_25
Z_Stack_24_08_2022_Day5_Dish0_random_location_26
Z_Stack_24_08_2022_Day5_Dish0_random_location_27
Z_Stack_24_08_2022_Day5_Dish0_random_location_28
```

Therefore, they should be evaluated on the same data.

### Checkpoints

There are checkpoints for the pre-trained models. The total training time is 400 epochs. For each of the approaches, there is an
encoder and decoder at epochs 100, 200 and 300 in the ``./Checkpoints`` folder. They are labelled in the following way ``encoder_{type}_e{epoch}``. 
The normal manifold-based models have an empty type, just like in the ``./ModelSaves`` folder.

### ML-VAE Score

The ML-VAE score measures the level of disentanglement. The files to calculate and visualize the ML-VAE score are stored in
the ``./Evaluation/ML_VAE_score`` folder. After calculating the ML-VAE score with one of the files, the results can be visualized. 
In visualize_ml_vae_score.py, a training monitor resulting from this calculation can be visualized.

For the three pretrained models, the ML-VAE scores have already been calculated. These monitors are stored in the ``./MLVAE-scores`` folder.

### Reconstruction

The reconstruction capabilities of the models can be evaluated qualitatively and quantitatively. 
All files for evaluating the reconstruction capabilities are stored in ``./Evaluation/Reconstruction``

#### Qualitatively

The qualitative evaluation for the reconstruction can be found in the files titled ``qualitative___.py`` 
In the files, an image is chosen from the data in the ``./EvalData`` 
It is encoded and decoded. The input and reconstruction are plotted.

#### Quantitatively

The quantitative evaluation for the reconstruction can be found in the files titled ``quantitative___.py``
The dataset is split in test and training set. For both sets, the inputs are compared to the reconstructions, resulting in PSNR and SSIM scores.

### Deblurring 

The deblurring capabilities of the models can be evaluated qualitatively and quantitatively. 
All files for evaluating the deblurring capabilities are stored in ``./Evaluation/Deblurring`` 

#### Qualitatively

The qualitative evaluation for deblurring can be found in the files titled ``qualitative___.py``
In the files, a blurry image is chosen from the data in the ``./EvalData`` folder
Furthermore, an in-focus image is chosen from the data in the same folder
The blur latent code from the in-focus image is combined with the identity code of the blurry image.
This combination gets decoded, leading to an in-focus reconstruction
The input, in-focus image, reconstruction and expected image are plotted.

#### Quantitatively

The quantitative evaluation for deblurring can be found in the files titled ``quantitative___.py``
The dataset is split in test and training set. For all images in both sets, the same deblurring procedure as described in the previous section is performed.
This results again in PSNR and SSIM scores when comparing the deblurring attempt with the target in-focus image.

### Interpolation

In order to investigate the latent spaces, we interpolate linearly on the blur latent space to see if an in-focus image is found. 
These files are stored in the ``./Evaluation/Interpolation`` folder.

### Visualizing the latent space

Visualizing the latent space can be done using tSNE plots. The files to generate these plots for the identity and blur latent code are in the ``./Evaluation/tSNE`` folder
