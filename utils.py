"""
Functionalities used throughout the training process
"""

# Third party imports
import math
import os
import torch
import numpy as np
from PIL import Image
import torchvision
from torch import nn
import cv2

dirname = os.path.dirname(__file__)

folder_brightfield = "./CroppedData/Brightfield"
folder_fluorescent = "./CroppedData/Fluorescent"


def group_wise_reparameterize_each(training, mu, logvar, labels_batch, list_groups_labels, cuda):
    """
    Method to group-wise apply the reparameterization trick to the latent variables
    IN :
        - Boolean flag whether or not we are in a training process (training, boolean)
        - Set of mu values for the latent variables per image (mu, pytorch tensor)
        - Set of logvars for the latent variables per image (logvar, pytorch tensor)
        - The labels for each of the images(labels_batch, list)
        - A list of the unique labels (list_group_labels, list)
        - Boolean flag whether or not we are running pytorch on cuda (cuda, boolean)
    OUT:
        - The latent embeddings for all images (identity_samples, pytorch tensor)
        - The sorting indexes at which the latent embeddings are sorted (indexes, list)
        - The sizes of the of the different groups (sizes, list)
    """
    if training:
        std = logvar.mul(0.5).exp_()
    else:
        std = torch.zeros_like(logvar)

    identity_samples = []
    indexes = []
    sizes = []

    for k, g in enumerate(list_groups_labels):
        samples_group = [i for i, x in enumerate(labels_batch) if x == g]
        size_group = len(samples_group)
        if size_group > 0:
            if cuda:
                eps = torch.cuda.FloatTensor(size_group, std.size(1)).normal_()
            else:
                eps = torch.FloatTensor(size_group, std.size(1)).normal_()
            group_identity_sample = std[k][None, :] * eps + mu[k][None, :]
            identity_samples.append(group_identity_sample)
            indexes.append(torch.LongTensor(samples_group))
            size_group = torch.ones(size_group) * size_group
            sizes.append(size_group)

    identity_samples = torch.cat(identity_samples, dim=0)
    indexes = torch.cat(indexes)
    sizes = torch.cat(sizes)

    id_index_list = indexes.tolist()
    reordered_index_list = []
    for i in range(len(id_index_list)):
        reordered_index_list.append(id_index_list.index(i))

    new_index_list = torch.tensor(reordered_index_list)

    identity_latent_embeddings = identity_samples[new_index_list, :]

    return identity_latent_embeddings, indexes, sizes


def reparameterize(training, mu, logvar):
    """
    Method to apply the reparameterization trick
    IN:
        - Boolean flag whether or not we are in a training process (training, boolean)
        - Set of mu values for the latent variables per image (mu, pytorch tensor)
        - Set of logvar values for the latent variables per image (logvar, pytorch tensor)

    OUT:
        - Latent embeddings (pytorch tensor)
    """
    if training:
        std = logvar.mul(0.5).exp_()
        eps = torch.zeros_like(std).normal_()
        return eps.mul(std).add_(mu)
    else:
        return mu


def get_focus_level(dir):
    """
    Method used to get the in-focus level of a set of images with different z-levels
    :param dir:
        - A complete path to a directory containing the images (string)
    :return:
        - The z-level of the in-focus image (focus_level, int)
    """
    focus_level = 0
    least = 100
    for subdir, cropdirs, files in os.walk(dir):
        in_focus_img = cv2.imread(os.path.join(dir, "InFocus.tif"))
        for f in files:
            if str(f).startswith("Projection") or str(f).startswith("InFocus"):
                continue
            else:
                img = cv2.imread(os.path.join(dir, f))
                error = (np.square(in_focus_img - img)).mean()
                if error < least:
                    focus_level = int(f[:-4])
                    least = error
    return focus_level

def get_path_dicts_and_labels_and_focus_list(choice):
    """
    Method to get the path dictionaries and labels to instantiate the Dataset
    :param choice:
        - Absolute path to the directory containing the cropped and sorted data (string)
    :return:
        - Dictionary containing the path to the data folder for a certain image and crop
          Follows the structure dict[label] -> path
        - Dictionary containing the path to the in focus image for a certain label
          Follows the structure dict[label] -> path
        - List of all labels existent in the dataset: ["{day}-{crop}"]
        - List of all the in-focus z-levels for each label [int]
    """
    if choice == "brightfield":
        folder = folder_brightfield
    else:
        folder = folder_fluorescent

    path_dict = {}
    in_focus_path_dict = {}
    labels = []
    focus_list = []

    for day in os.listdir(os.path.join(dirname, folder)):
        print(day)
        # Loop through all chops for that day and location
        for chop in os.listdir(os.path.join(dirname, folder, day)):
            crop = chop.replace('h', 'r')
            label = day + "-" + crop
            data_path = os.path.join(dirname, folder, day, chop, "data")
            target_path = os.path.join(dirname, folder, day, chop, "target")
            in_focus_path_dict[label] = os.path.join(target_path, "InFocus.tif")
            with open(os.path.join(target_path, "z_level_focus.txt")) as file:
                in_focus_level = int(file.read())
            paths = []
            for f in os.listdir(data_path):
                paths.append(os.path.join(data_path, f))
            path_dict[label] = paths
            labels.append(label)
            focus_list.append(in_focus_level)

    return path_dict, in_focus_path_dict, labels, focus_list


def preprocess_image(path):
    """
    Method used to read an image and convert it to a pytorch tensor
    :param path:
        - Absolute path to the image
    :return:
        - A 1-dimensional pytorch tensor of the image
    """
    img = Image.open(path).convert('L')
    img = torchvision.transforms.ToTensor()(img)
    return img


def preprocess_fluo_image(path):
    """
    Method used to read a fluorescent image (bluescale) and convert it to a pytorch tensor
    :param path:
        - Absolute path to the image
    :return:
        - A 1-dimensional pytorch tensor of the image
    """
    img = Image.open(path).convert('RGB')
    img = torchvision.transforms.ToTensor()(img)
    blues = img[2, :]
    return blues[None, :]


def accumulate_group_evidence(mu, logvar, labels_batch):
    """
    Method used to accumulate group evidence following the ML-VAE principle
    :param mu:
        - Pytorch tensor containing mu values for each latent variable for each image
    :param logvar:
        - Pytorch tensor containing logvar values for each latent variable for each image
    :param labels_batch:
        - Labels for all entries in the mu and logvar values
    :return:
        - Pytorch tensor containing mu values for each group
        - Pytorch tensor containing logvar values for each group
        - List of labels per group
        - List of sizes per group
    """
    group_identity_mu = []
    group_identity_inv_logvar = []
    list_groups_labels = []
    sizes_group = []
    groups = list(set(list(labels_batch)))
    labels_batch = list(labels_batch)

    for group_label in groups:
        samples_group = [i for i, x in enumerate(labels_batch) if x == group_label]

        if len(samples_group) > 0:
            inv_group_logvar = - logvar[samples_group, :]
            inv_group_var = torch.exp(inv_group_logvar)
            group_mu = mu[samples_group, :] * inv_group_var

            if len(samples_group) > 1:
                group_mu = group_mu.sum(0, keepdim=True)
                inv_group_logvar = torch.logsumexp(inv_group_logvar, dim=0, keepdim=True)
            else:
                group_mu = group_mu[None, :]
                inv_group_logvar = inv_group_logvar[None, :]

            group_identity_mu.append(group_mu)
            group_identity_inv_logvar.append(inv_group_logvar)
            list_groups_labels.append(group_label)
            sizes_group.append(len(samples_group))

    group_identity_mu = torch.cat(group_identity_mu, dim=0)
    group_identity_inv_logvar = torch.cat(group_identity_inv_logvar, dim=0)
    sizes_group = torch.FloatTensor(sizes_group)
    # inverse log variance
    group_identity_logvar = - group_identity_inv_logvar
    # multiply group var with group log variance
    group_identity_mu = group_identity_mu * torch.exp(group_identity_logvar)

    return group_identity_mu, group_identity_logvar, list_groups_labels, sizes_group


def weights_initialization(layer):
    if isinstance(layer, nn.Linear):
        lecun_normal_(layer.bias)
        lecun_normal_(layer.weight)


def lecun_normal_(tensor, gain=1):
    r"""Adapted from https://pytorch.org/docs/0.4.1/_modules/torch/nn/init.html#xavier_normal_
    """
    dimensions = tensor.size()
    if len(dimensions) == 1:  # bias
        fan_in = tensor.size(0)
    elif len(dimensions) == 2:  # Linear
        fan_in = tensor.size(1)
    else:
        num_input_fmaps = tensor.size(1)
        if tensor.dim() > 2:
            receptive_field_size = tensor[0][0].numel()
        fan_in = num_input_fmaps * receptive_field_size

    std = gain * math.sqrt(1.0 / (fan_in))
    with torch.no_grad():
        return tensor.normal_(0, std)


def make_directory_if_not_exists(path):
    """
    Method used to create a directory if it does not exist yet
    :param path:
        - Absolute path of the directory that should be created
    """
    if not os.path.exists(path):
        os.mkdir(path)


def check_if_crop_empty(crop, type):
    """
    Method to check if a crop is "empty" e.g. the image shows nothing
    :param crop:
        - Numpy array containing the pixel values for the crop
    :param type:
        - String containing the type of images
    :return:
        - Boolean, whether the crop is empty or not
    """
    if type == 'Fluorescent':
        return np.max(crop) < 35
    else:
        return np.min(crop) > 102 and np.max(crop) < 153


def get_beta_dict():
    """
    Method used to create beta parameter used for delaying losses in manifold-based approach
    :return:
        - Dictionary of beta values per epoch
    """
    beta = 0.001
    betas = []
    indexes = []
    mul = 1.1
    max_reached = False
    beta_dict = {}

    for i in range(0, 101):
        indexes.append(i)
        if not max_reached and i > 30:
            mul = mul ** 1.05
            beta = mul * beta
            if beta > 1:
                beta = 1
                max_reached = True
        betas.append(beta)
        beta_dict[i] = torch.tensor([beta])

    return beta_dict


def latent_distance(v_1, v_2):
    avg_length = (torch.norm(v_1) + torch.norm(v_2)) / 2
    dist = torch.linalg.norm(v_1 - v_2)
    return dist / avg_length


def trio_process(X_1, X_2, X_3, encoder, decoder, loss):
    """
    Method used to calculate losses for manifold-based approaches
    :param X_1:
        - Images which share ID with X_2 and blur level with X_3
    :param X_2:
        - Images which share ID with X_1
    :param X_3:
        - Images which share blur level with X_1
    :param encoder:
        - Encoder model
    :param decoder:
        - Decoder model
    :param loss:
        - Loss function for image comparison
    :return:
        - Reconstruction loss, consistency loss, contrastive loss, cross reconstruction loss
    """
    m = torch.tensor([0.6])

    # X_1, X_2 share id, different blur
    # X_1, X_3 share blur, different id

    blur_code_1, id_code_1 = encoder(X_1)
    blur_code_2, id_code_2 = encoder(X_2)
    blur_code_3, id_code_3 = encoder(X_3)

    recon_1 = decoder(blur_code_1, id_code_1)
    recon_2 = decoder(blur_code_2, id_code_2)
    recon_3 = decoder(blur_code_3, id_code_3)

    # Calculate normal reconstruction loss
    rec_loss_1 = loss(X_1, recon_1)
    rec_loss_2 = loss(X_2, recon_2)
    rec_loss_3 = loss(X_3, recon_3)

    recon_loss = rec_loss_1 + rec_loss_2 + rec_loss_3

    # Calculate consistency loss
    # Keep identity, vary blur level
    recon_cons_1 = decoder(blur_code_2, id_code_1)
    blur_cons_1, id_code_1_prime = encoder(recon_cons_1)

    cons_loss_term_1 = latent_distance(id_code_1_prime, id_code_1)

    # Keep blur level, vary identity
    recon_cons_2 = decoder(blur_code_1, id_code_3)
    blur_code_1_prime, id_cons_2 = encoder(recon_cons_2)
    cons_loss_term_2 = latent_distance(blur_code_1_prime, blur_code_1)

    cons_loss = torch.tensor([0.1]) * (cons_loss_term_1 + cons_loss_term_2)

    # Calculate constrastive/distance loss
    dist_loss_term_1 = latent_distance(id_code_1, id_code_2)

    dist_loss_term_2 = torch.max(m - latent_distance(blur_code_1, blur_code_2), torch.zeros(1))

    dist_loss_term_3 = latent_distance(blur_code_1, blur_code_3)
    dist_loss_term_4 = torch.max(m - latent_distance(id_code_1, id_code_3), torch.zeros(1))

    dist_loss = dist_loss_term_1 + dist_loss_term_2 + dist_loss_term_3 + dist_loss_term_4

    # Calculate Cross recon loss
    cross_rec_loss_1 = loss(X_1, decoder(blur_code_3, id_code_1))
    cross_rec_loss_2 = loss(X_3, decoder(blur_code_1, id_code_3))
    cross_rec_loss_3 = loss(X_1, decoder(blur_code_1, id_code_2))
    cross_rec_loss_4 = loss(X_2, decoder(blur_code_2, id_code_1))

    cross_rec_loss = cross_rec_loss_1 + cross_rec_loss_2 + cross_rec_loss_3 + cross_rec_loss_4

    return recon_loss, cons_loss, dist_loss, cross_rec_loss


def get_path_dict_and_labels(choice):
    if choice == "brightfield":
        folder = folder_brightfield
    else:
        folder = folder_fluorescent

    path_dict = {}
    labels = []

    for day in os.listdir(os.path.join(dirname, folder)):
        # Loop through all chops for that day and location
        for chop in os.listdir(os.path.join(dirname, folder, day)):
            chop_name = chop.replace('h', 'r')
            label = day + "_" + chop_name
            data_path = os.path.join(dirname, folder, day, chop, "data")
            for f in os.listdir(data_path):
                file_label = label + "_" + f[:-4]
                labels.append(file_label)
                path_dict[file_label] = os.path.join(data_path, f)
    return path_dict, labels


def get_grouped_path_dict(choice):
    if choice == "brightfield":
        folder = folder_brightfield
    else:
        folder = folder_fluorescent

    path_dict = {}
    labels = []

    for day in os.listdir(os.path.join(dirname, folder)):
        print(day)
        # Loop through all chops for that day and location
        for chop in os.listdir(os.path.join(dirname, folder, day)):
            label = day + "-" + chop
            data_path = os.path.join(dirname, folder, day, chop, "data")
            paths = []
            for f in os.listdir(data_path):
                paths.append(os.path.join(data_path, f))
            path_dict[label] = paths
            labels.append(label)

    return path_dict, labels

def get_path_dict_and_labels_and_focus_list(choice):
    if choice == "brightfield":
        folder = folder_brightfield
    else:
        folder = folder_fluorescent

    path_dict = {}
    labels = []
    focus_list = []

    for day in os.listdir(os.path.join(dirname, folder)):
        print(day)
        # Loop through all chops for that day and location
        for chop in os.listdir(os.path.join(dirname, folder, day)):
            label = day + "-" + chop
            data_path = os.path.join(dirname, folder, day, chop, "data")
            target_path = os.path.join(dirname, folder, day, chop, "target")
            with open(os.path.join(target_path, "z_level_focus.txt")) as file:
                in_focus_level = int(file.read())
            paths = []
            for f in os.listdir(data_path):
                # z_level = int(f[:-4])
                # focus_diff = z_level - in_focus_level
                # focus_list.append(focus_diff)

                paths.append(os.path.join(data_path, f))
            path_dict[label] = paths
            labels.append(label)
            focus_list.append(in_focus_level)

    return path_dict, labels, focus_list