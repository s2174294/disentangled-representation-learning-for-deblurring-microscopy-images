from collections import OrderedDict
import torch.nn as nn


class Classifier(nn.Module):
    def __init__(self, dim, num_classes):
        super(Classifier, self).__init__()

        self.model = nn.Sequential(OrderedDict([
            ('lay_1', nn.Linear(in_features=dim, out_features=2048, bias=True)),
            ('bn_1', nn.BatchNorm1d(num_features=2048)),
            ('tan_1', nn.Tanh()),

            ('lay_2', nn.Linear(in_features=2048, out_features=num_classes, bias=True)),
            ('bn_2', nn.BatchNorm1d(num_features=num_classes)),
            ('som_2', nn.Softmax())
        ]))


    def forward(self, x):
        x = self.model(x)
        return x
