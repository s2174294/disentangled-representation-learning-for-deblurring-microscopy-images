from torch.utils.data import DataLoader
import argparse
from Models.MLVAE import VAEEncoder, VAEDecoder

from CustomDataset.SampleDataset import SampleDataset
from Models.vgg import VGGLoss
import torch
import torch.optim as optim
import os

from utils import weights_initialization, get_path_dict_and_labels_and_focus_list, reparameterize, \
    accumulate_group_evidence, group_wise_reparameterize_each

savedmodels = os.path.join(os.getcwd(), "../TrainedModels")


def process(SETTINGS, X, labels_batch, focus_diffs, encoder, decoder, epoch, loss):
    if SETTINGS.cuda:
        blur_mu, blur_logvar, identity_mu, identity_logvar = encoder(X.cuda())
    else:
        blur_mu, blur_logvar, identity_mu, identity_logvar = encoder(X)

    blur_latent_embeddings = reparameterize(training=True, mu=blur_mu, logvar=blur_logvar)

    group_mu, group_logvar, list_g, sizes_group = accumulate_group_evidence(identity_mu, identity_logvar,
                                                                            labels_batch)

    identity_latent_embeddings, indexes, sizes = group_wise_reparameterize_each(training=True, mu=group_mu,
                                                                                logvar=group_logvar,
                                                                                labels_batch=labels_batch,
                                                                                list_groups_labels=list_g,
                                                                                cuda=SETTINGS.cuda)

    blur_kl_loss = torch.mean(- 0.5 * torch.sum(1 + blur_logvar - blur_mu.pow(2) - blur_logvar.exp(), dim=1))
    id_kl_loss = torch.mean(- 0.5 * torch.sum(1 + group_logvar - group_mu.pow(2) - group_logvar.exp(), dim=1))

    img = decoder(blur_latent_embeddings, identity_latent_embeddings)

    rec_loss = loss(X, img)

    reconstruction_loss = rec_loss

    elbo = torch.tensor(100) * reconstruction_loss + torch.tensor(0.01) * blur_kl_loss + torch.tensor(
        0.001) * id_kl_loss

    return elbo, torch.tensor(100) * reconstruction_loss, torch.tensor(0.01) * blur_kl_loss, torch.tensor(
        0.001) * id_kl_loss, blur_latent_embeddings


def training(SETTINGS, RUN_ID):
    """
    Main training method
    """
    encoder = VAEEncoder()
    encoder.apply(weights_initialization)

    decoder = VAEDecoder()
    decoder.apply(weights_initialization)

    VGG_loss = VGGLoss()

    if SETTINGS.cuda:
        encoder.cuda()
        decoder.cuda()
        VGG_loss.cuda()

    """
    Optimizer
    """
    optimizer = optim.Adam(
        list(encoder.parameters()) + list(decoder.parameters()),
        lr=SETTINGS.initial_learning_rate,
        betas=(SETTINGS.beta_1, SETTINGS.beta_2)
    )

    decayRate = 0.98
    lr_scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer=optimizer, gamma=decayRate)

    """
    Training
    """
    if torch.cuda.is_available() and not SETTINGS.cuda:
        print("WARNING: You have a CUDA device, so you should probably run with --cuda")

    print("Loading dataset")

    path_dict, labels, focus_list = get_path_dict_and_labels_and_focus_list("brightfield")

    data = SampleDataset(path_dict, labels, 25, focus_list, -70, 70)

    dataset_size = len(data)

    print(f"Loaded dataset with {dataset_size} groups")

    indices = list(range(dataset_size))
    split = int(0.2 * dataset_size)

    train_indices, test_indices = indices[split:], indices[:split]
    train_data = torch.utils.data.Subset(data, train_indices)

    kwargs = {}

    loader = DataLoader(train_data, batch_size=SETTINGS.batch_size, shuffle=True, **kwargs)
    training_monitor = torch.zeros(SETTINGS.end_epoch - SETTINGS.start_epoch, 4)

    for epoch in range(SETTINGS.start_epoch, SETTINGS.end_epoch):
        print('')
        print(f'Epoch #{epoch} ..........................................................................')
        elbo_epoch = 0
        term1_epoch = 0
        term2_epoch = 0
        term3_epoch = 0

        for it, (image_batch, labels_batch, focus_diffs_batch) in enumerate(loader):
            if it % 5 == 0 or epoch == 0:
                print(f'Train iteration {it}')

            if SETTINGS.cuda:
                X = torch.cat(image_batch, dim=0).cuda()
                focus_diffs = torch.cat(focus_diffs_batch, dim=0).cuda()
            else:
                X = torch.cat(image_batch, dim=0)
                focus_diffs = torch.cat(focus_diffs_batch, dim=0)

            labels_batch = [el for tuple in labels_batch for el in tuple]

            elbo, reconstruction_proba, blur_kl_divergence_loss, identity_kl_divergence_loss, blur_latent_code = \
                process(SETTINGS, X, labels_batch, focus_diffs, encoder, decoder, epoch, VGG_loss)

            optimizer.zero_grad()
            (elbo).backward()
            optimizer.step()

            elbo_epoch += elbo.detach()
            term1_epoch += reconstruction_proba.detach()
            term2_epoch += blur_kl_divergence_loss.detach()
            term3_epoch += identity_kl_divergence_loss.detach()

        print("VAE LR: ", lr_scheduler.get_last_lr())
        if epoch % 5 == 0:
            lr_scheduler.step()

        print("Elbo epoch %.2f" % (elbo_epoch / (it + 1)))
        print("Rec. Loss %.4f" % (term1_epoch / (it + 1)))
        print("KL blur %.2f" % (term2_epoch / (it + 1)))
        print("KL identity %.2f" % (term3_epoch / (it + 1)))

        training_monitor[epoch, :] = torch.FloatTensor(
            [elbo_epoch / (it + 1), term1_epoch / (it + 1), term2_epoch / (it + 1), term3_epoch / (it + 1)])

        if os.path.exists(os.path.join(savedmodels, RUN_ID + f'_training_monitor_e{epoch - 1}')):
            os.remove(os.path.join(savedmodels, RUN_ID + f'_training_monitor_e{epoch - 1}'))
        torch.save(training_monitor, os.path.join(savedmodels, RUN_ID + '_training_monitor_e%d' % epoch))

        if (epoch + 1) % 10 == 0 or (epoch + 1) == SETTINGS.end_epoch:
            print("SAVE")
            torch.save(encoder.state_dict(),
                       os.path.join(savedmodels, RUN_ID + '_' + SETTINGS.encoder_save + f'_e{epoch}'))
            torch.save(decoder.state_dict(),
                       os.path.join(savedmodels, RUN_ID + '_' + SETTINGS.decoder_save + f'_e{epoch}'))

parser = argparse.ArgumentParser()

# add arguments
parser.add_argument('--cuda', type=bool, default=False, help="run the following code on a GPU")
parser.add_argument('--batch_size', type=int, default=3, help="batch size for training")

parser.add_argument('--initial_learning_rate', type=float, default=0.001, help="starting learning rate")
parser.add_argument('--beta_1', type=float, default=0.9, help="default beta_1 val for adam")
parser.add_argument('--beta_2', type=float, default=0.999, help="default beta_2 val for adam")

# paths to save models
parser.add_argument('--encoder_save', type=str, default='encoder', help="model save for encoder")
parser.add_argument('--decoder_save', type=str, default='decoder', help="model save for decoder")
parser.add_argument('--start_epoch', type=int, default=0, help="flag to set the starting epoch for training")
parser.add_argument('--end_epoch', type=int, default=400, help="flag to indicate the final epoch of training")

flags = parser.parse_args(args=[])
run_id = "test_run_mani_1d"

if __name__ == "__main__":
    training(flags, run_id)