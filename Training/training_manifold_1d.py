from torch.utils.data import DataLoader, RandomSampler
from CustomDataset.TrioDataset import TrioDataset
from Models.manifold_one_dimensional import Encoder, Decoder
from Models.vgg import VGGLoss
import argparse
import torch.optim as optim
import torch
import os

from utils import weights_initialization, get_grouped_path_dict, get_beta_dict, trio_process

savedmodels = os.path.join(os.getcwd(), "../TrainedModels")


def training(SETTINGS, RUN_ID):
    """
    Main training method
    """
    encoder = Encoder()
    encoder.apply(weights_initialization)

    decoder = Decoder()
    decoder.apply(weights_initialization)

    VGG_loss = VGGLoss()

    if SETTINGS.cuda:
        encoder.cuda()
        decoder.cuda()
        VGG_loss.cuda()

    """
    Optimizer
    """
    optimizer = optim.Adam(
        list(encoder.parameters()) + list(decoder.parameters()),
        lr=SETTINGS.initial_learning_rate,
        betas=(SETTINGS.beta_1, SETTINGS.beta_2)
    )

    decayRate = 0.98
    lr_scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer=optimizer, gamma=decayRate)

    """
    Training
    """
    if torch.cuda.is_available() and not SETTINGS.cuda:
        print("WARNING: You have a CUDA device, so you should probably run with --cuda")

    print("Loading dataset")

    path_dict, labels = get_grouped_path_dict("brightfield")
    beta_dict = get_beta_dict()
    data = TrioDataset(path_dict, labels)
    dataset_size = len(data)

    print(f"Loaded dataset with {dataset_size} images")

    indices = list(range(dataset_size))
    split = int(0.2 * dataset_size)

    train_indices, val_indices = indices[split:], indices[:split]
    train_data = torch.utils.data.Subset(data, train_indices)
    # val_data = torch.utils.data.Subset(data, val_indices)

    kwargs = {}

    sampler = RandomSampler(train_data, replacement=True, num_samples=1000)
    loader = DataLoader(train_data, sampler=sampler, batch_size=SETTINGS.batch_size)

    training_monitor = torch.zeros(SETTINGS.end_epoch - SETTINGS.start_epoch, 5)
    # eval_monitor = torch.zeros(SETTINGS.end_epoch - SETTINGS.start_epoch, 5)

    for epoch in range(SETTINGS.start_epoch, SETTINGS.end_epoch):
        print('')
        print(f'Epoch #{epoch} ..........................................................................')
        elbo_epoch = 0
        term1_epoch = 0
        term2_epoch = 0
        term3_epoch = 0
        term4_epoch = 0
        beta = beta_dict[epoch]

        for it, (X_1, X_2, X_3, f_1, f_2, f_3, l_1, l_2) in enumerate(loader):
            if it % 5 == 0 or epoch == 1:
                print(f'Train iteration {it}')

            if SETTINGS.cuda:
                X_1 = X_1.cuda()
                X_2 = X_2.cuda()
                X_3 = X_3.cuda()

            l_rec, l_cons, l_dist, l_cross_rec = trio_process(X_1, X_2, X_3, encoder, decoder, VGG_loss)

            elbo = l_rec + beta * l_cons + beta * l_dist + beta * l_cross_rec

            optimizer.zero_grad()
            (elbo).backward()
            optimizer.step()

            elbo_epoch += elbo.detach()
            term1_epoch += l_rec.detach()
            term2_epoch += l_cons.detach()
            term3_epoch += l_dist.detach()
            term4_epoch += l_cross_rec.detach()

        print("Learning Rate: ", lr_scheduler.get_last_lr())

        if epoch % 5 == 0:
            lr_scheduler.step()

        print("Elbo epoch %.4f" % (elbo_epoch / (it + 1)))
        print("Rec. Loss %.4f" % (term1_epoch / (it + 1)))
        print("Cons. Loss %.4f" % (term2_epoch / (it + 1)))
        print("Dist. Loss %.4f" % (term3_epoch / (it + 1)))
        print("Cross Rec. Loss %.4f" % (term4_epoch / (it + 1)))
        print(f"Beta: {beta}")

        training_monitor[epoch, :] = torch.FloatTensor([elbo_epoch / (it + 1), term1_epoch / (it + 1), term2_epoch / (it + 1), term3_epoch / (it + 1), term4_epoch / (it + 1)])

        # Remove previous training monitor
        if os.path.exists(os.path.join(savedmodels, RUN_ID + f'_training_monitor_e{epoch - 1}')):
            os.remove(os.path.join(savedmodels, RUN_ID + f'_training_monitor_e{epoch - 1}'))

        # Save current training monitor
        torch.save(training_monitor, os.path.join(savedmodels, RUN_ID + '_training_monitor_e%d' % epoch))

        if (epoch + 1) % 10 == 0 or (epoch + 1) == SETTINGS.end_epoch:
            print("SAVE")
            torch.save(encoder.state_dict(),
                       os.path.join(savedmodels, RUN_ID + '_' + SETTINGS.encoder_save + f'_e{epoch}'))
            torch.save(decoder.state_dict(),
                       os.path.join(savedmodels, RUN_ID + '_' + SETTINGS.decoder_save + f'_e{epoch}'))

parser = argparse.ArgumentParser()

# add arguments
parser.add_argument('--cuda', type=bool, default=False, help="run the following code on a GPU")
parser.add_argument('--batch_size', type=int, default=12, help="batch size for training")

parser.add_argument('--initial_learning_rate', type=float, default=0.001, help="starting learning rate")
parser.add_argument('--beta_1', type=float, default=0.9, help="default beta_1 val for adam")
parser.add_argument('--beta_2', type=float, default=0.999, help="default beta_2 val for adam")

# paths to save models
parser.add_argument('--encoder_save', type=str, default='encoder', help="model save for encoder")
parser.add_argument('--decoder_save', type=str, default='decoder', help="model save for decoder")
parser.add_argument('--start_epoch', type=int, default=0, help="flag to set the starting epoch for training")
parser.add_argument('--end_epoch', type=int, default=400, help="flag to indicate the final epoch of training")

flags = parser.parse_args(args=[])
run_id = "test_run_mani_1d"

if __name__ == "__main__":
    training(flags, run_id)
