"""
File containing all methods relevant for data sorting, cropping and preprocessing
"""

# Third party imports
import os
import shutil
import re
from PIL import Image
import cv2

# First party imports
from utils import get_focus_level, make_directory_if_not_exists, check_if_crop_empty

"""
Directories: 
    data_dir  : Original dataset
    sorted_dir: Sorted and resized dataset
    cropped_dir  : Data per crop
"""
data_dir = "./Dataset"
sorted_dir = "./SortedData"
cropped_dir = "./CroppedData"

crop_width = 128
crop_height = 128


def sort_and_resize_data():
    """
    Method used to load and sort the data into a desirable structure
    Furthermore, resize the images in the dataset from 2048x1536 to 1024x768
    Images are stored in the sorted_dir directory
    """
    brightfield_dir = os.path.join(sorted_dir, "Brightfield")
    fluorescent_dir = os.path.join(sorted_dir, "Fluorescent")
    make_directory_if_not_exists(brightfield_dir)
    make_directory_if_not_exists(fluorescent_dir)
    for subdir, dirs, files in os.walk(data_dir):
        for dir in dirs:
            date = re.findall(r"[0-9]{2}_[0-9]{2}_[0-9]{4}", dir)[0]
            loc = re.findall(r"location_[0-9]*", dir)[0]
            dirname = date + '_' + loc

            br_path = os.path.join(brightfield_dir, dirname)
            fl_path = os.path.join(fluorescent_dir, dirname)

            # MAKE DIRECTORIES IF NOT EXISTS
            if not os.path.exists(br_path):
                os.mkdir(br_path)
            if not os.path.exists(fl_path):
                os.mkdir(fl_path)

            dir_loaded = os.path.join(data_dir, dir)

            for sub, ds, fil in os.walk(dir_loaded):
                for f in fil:
                    # DAPI indicates a fluorescent image
                    if f.startswith("Z-Stack - DAPI"):
                        new_filename = f.split(" ")[-1]
                        if not os.path.exists(os.path.join(fl_path, new_filename)):
                            img = Image.open(os.path.join(dir_loaded, f))
                            new_img = img.resize((1024, 768))
                            new_img.save(os.path.join(fl_path, new_filename))
                    # Trans indicates a brightfield image
                    if f.startswith("Z-Stack - Trans"):
                        new_filename = f.split(" ")[-1]
                        if not os.path.exists(os.path.join(br_path, new_filename)):
                            img = Image.open(os.path.join(dir_loaded, f))
                            new_img = img.resize((1024, 768))
                            new_img.save(os.path.join(br_path, new_filename))
    print("Finished sorting")


def image_crop():
    """
    Method used to crop the images in the resize_dir into multiple images of 128x128
    Cropped images get stored in the in the cropped_dir:
        - cropped_dir:
            - Brightfield:
                - day_1:
                    - crop_0
                    - crop_1
                    - ...
                - day_2
                    - crop_0
                    - crop_1
                    - ...
    For each crop, two folders are generated:
        - a data folder with images at all z-levels
        - a target folder containing the in-focus image and its z-level
    """
    for sort_subdirs, sort_dirs, sort_files in os.walk(sorted_dir):
        for sort_dir in sort_dirs:
            cropped_sub_path = os.path.join(cropped_dir, sort_dir)
            sorted_sub_path = os.path.join(sorted_dir, sort_dir)
            make_directory_if_not_exists(cropped_sub_path)
            for subdir, dirs, files in os.walk(sorted_sub_path):
                for dir in dirs:
                    dir_loaded = os.path.join(sorted_sub_path, dir)
                    in_focus_level = get_focus_level(dir_loaded)
                    make_directory_if_not_exists(os.path.join(cropped_sub_path, dir))
                    for sub, ds, fls in os.walk(dir_loaded):
                        for f in fls:
                            img = cv2.imread(os.path.join(dir_loaded, f))
                            crops = [img[x:x + crop_width, y:y + crop_height] for x in range(0, img.shape[0], crop_width) for y
                                     in range(0, img.shape[1], crop_height)]
                            for index, crop in enumerate(crops):
                                crop_dir = os.path.join(cropped_sub_path, dir, "crop_" + str(index))
                                data_dir = os.path.join(crop_dir, "data")
                                target_dir = os.path.join(crop_dir, "target")
                                make_directory_if_not_exists(crop_dir)
                                make_directory_if_not_exists(data_dir)
                                make_directory_if_not_exists(target_dir)

                                if str(f).startswith("Projection") or str(f).startswith("InFocus"):
                                    filename = os.path.join(target_dir, str(f))
                                    cv2.imwrite(filename, crop)

                                elif not check_if_crop_empty(crop, sort_dir):
                                    focus_level = int(f[:-4]) - in_focus_level
                                    new_filename = os.path.join(data_dir, str(focus_level) + ".tif")
                                    cv2.imwrite(new_filename, crop)

                                # Store the z_level in the data directory per crop
                                if not os.path.exists(os.path.join(target_dir, "z_level_focus.txt")):
                                    with open(os.path.join(target_dir, "z_level_focus.txt"), 'w') as z_level:
                                        z_level.write(str(in_focus_level))
    print("Finished cropping")


def remove_small_crops():
    """
    Remove crops with less then 32 non-empty data points
    Method used to make sure we do not overfit the model with empty crops
    """
    for type_sub, type_dirs, type_files in os.walk(cropped_dir):
        for type_dir in type_dirs:
            # Brightfield and Fluorescent
            type_dir_sub_path = os.path.join(cropped_dir, type_dir)
            for subdir, dirs, files in os.walk(type_dir_sub_path):
                for dir in dirs:
                    dirpath = os.path.join(type_dir_sub_path, dir)
                    for subdir, cropdirs, files in os.walk(dirpath):
                        for cropdir in cropdirs:
                            if cropdir.startswith("crop"):
                                if len(os.listdir(os.path.join(dirpath, cropdir, "data"))) < 40:
                                    shutil.rmtree(os.path.join(dirpath, cropdir))
                                elif len(os.listdir(os.path.join(dirpath, cropdir, "target"))) == 1:
                                    shutil.rmtree(os.path.join(dirpath, cropdir))


def create_folder_structure():
    """
    Method used to create the necessary folders
    """
    make_directory_if_not_exists(sorted_dir)
    make_directory_if_not_exists(cropped_dir)


if __name__ == '__main__':
    create_folder_structure()
    sort_and_resize_data()
    image_crop()
    remove_small_crops()
